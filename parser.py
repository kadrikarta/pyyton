#!/usr/bin/python2.7
#-*- coding: utf-8 -*-
#XML log parser

#impordime vajalikud moodulid
import os,re,time,sys,csv
from bs4 import BeautifulSoup

#defineerime logifaili
failinimi = os.path.basename(sys.argv[1])
logifail = os.popen('cat ' + sys.argv[1])

#Tagid mida taga otsime (kahes keeles)
header_est = ":id",":teenus", ":asutus",":allasutus",":isikukood"
header_eng = ":id",":service",":consumer",":unit",":userid"

start = time.time()

reanumber = 0
vigu = 0

#K2ime sisendfaili rida haaval l2bi
for rida in logifail.readlines():
	logistamp = rida[0:20]
	xml_data = rida[20:]
	isikukoode = 0
	reanumber = reanumber + 1
	sqlinsert = failinimi + "', '"  +  str(reanumber)
	#print xml_data
	soup = BeautifulSoup(xml_data, "lxml")
	header = soup.find(re.compile('header'))
	#print header
	body = soup.find(re.compile('body'))
	#print body

	if header.find(re.compile('userid')):
		userid = header.find(re.compile('userid')).string
		keel = header_eng
	elif header.find(re.compile('isikukood')):
		userid = header.find(re.compile('isikukood')).string
		keel = header_est
	else:
		vigu = vigu + 1
		continue
	#print userid

	#get header elements
	for row in csv.reader(keel):
		for item in row:
			item = item.strip('{}"') # koristab csv itemi jutum2rgid 2ra
			
			#proovib käsku, kui esineb viga, siis skript ei katkesta
			try:
				tag_sisu = header.find(re.compile(item)).string.rstrip()
				if tag_sisu[:2] == "EE":
					tag_sisu = tag_sisu[2:]	
					#print tag_sisu
				sqlinsert = sqlinsert + "', '" + tag_sisu
			except:
				sqlinsert = sqlinsert + "', '"
#			print sqlinsert
#end of header
#	print body

	for kisik in body.find_all("isikukood"):
		
		tag_sisu = kisik.string.rstrip()
#		print tag_sisu
		sqlinsertf = sqlinsert + "', '" + tag_sisu + "'"
		print sqlinsertf

	if isikukoode == 0:
		sqlinsertf = sqlinsert + "', '', '"
		print sqlinsertf	
end= time.time()
print end - start