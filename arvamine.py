#!/usr/bin/python
import random
arvatav = random.randint(1,100)
#print arvatav

print("Paku, milline arv on, vahemikus 1 - 100")

while True:
  sisend = int(input("Paku arv: "))

  if sisend == arvatav:
    print 'Arvasid 2ra! Oligi %i' % sisend
    break
  elif sisend < arvatav:
    print 'Otsitav arv on suurem kui %i' % sisend
  elif sisend > arvatav:
    print 'Otsitav arv on v2iksem kui %i' % sisend
  else:
    print 'Midagi l2ks vist valesti. Proovi uuesti.'
